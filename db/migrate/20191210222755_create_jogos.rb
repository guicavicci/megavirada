class CreateJogos < ActiveRecord::Migration[6.0]
  def change
    create_table :jogos do |t|

      t.integer :one
      t.integer :two
      t.integer :three
      t.integer :four
      t.integer :five
      t.integer :six

      t.timestamps
    end
  end
end
